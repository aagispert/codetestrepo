﻿using System.Collections.Generic;

namespace CodeTestConsoleApp
{
    public class Pizza
    {
        public List<string> Toppings { get; set; } = new List<string>();

        public override int GetHashCode()
        {
            int hash = 0;
            foreach (var topping in Toppings)
            {
                hash = hash ^ topping.GetHashCode();
            }
            return hash;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Pizza);
        }

        public bool Equals(Pizza pizza)
        {
            if(pizza is null)
            {
                return false;
            }

            if (Toppings.Count != pizza.Toppings.Count)
            {
                return false;
            }

            for (int i = 0; i < Toppings.Count; i++)
            {
                if(Toppings[i] != pizza.Toppings[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
