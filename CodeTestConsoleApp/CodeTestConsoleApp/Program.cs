﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace CodeTestConsoleApp
{
    class Program
    {
        static async Task Main()
        {
            var pizzas = await GetPizzas();
            var pizzaDict = CountPizzasByToppings(pizzas);
            var top20OrderedPizzas = GetTop20OrderedPizzas(pizzaDict);

            Print(top20OrderedPizzas);
        }

        private static async Task<List<Pizza>> GetPizzas()
        {
            const string ENDPOINT_URL = "https://www.brightway.com/CodeTests/pizzas.json";
            using var httpClient = new HttpClient();

            var response = await httpClient.GetAsync(ENDPOINT_URL);

            if (false == response.IsSuccessStatusCode)
            {
                throw new Exception($"{ENDPOINT_URL} is offline or we do not have connection to it.");
            }

            var responseJsonString = await response.Content.ReadAsStringAsync();
            var jsonSerializerOptions = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };
            var pizzas = JsonSerializer.Deserialize<List<Pizza>>(responseJsonString, jsonSerializerOptions);

            return pizzas;
        }

        private static Dictionary<Pizza, int> CountPizzasByToppings(List<Pizza> pizzas)
        {
            SortPizzaByToppings(pizzas);

            var pizzaDict = new Dictionary<Pizza, int>();
            foreach (var pizza in pizzas)
            {
                if (pizzaDict.ContainsKey(pizza))
                {
                    pizzaDict[pizza]++;
                }
                else
                {
                    pizzaDict[pizza] = 1;
                }
            }

            return pizzaDict;
        }

        private static void SortPizzaByToppings(List<Pizza> pizzas)
        {
            foreach (var pizza in pizzas)
            {
                pizza.Toppings.Sort();
            }
        }

        private static List<PizzaWrapper> GetTop20OrderedPizzas(Dictionary<Pizza, int> pizzaDict)
        {
            var pizzasWrapper = new List<PizzaWrapper>();
            foreach (var key in pizzaDict.Keys)
            {
                var pizzaWrapper = new PizzaWrapper
                {
                    Pizza = key,
                    OrderCount = pizzaDict[key]
                };

                pizzasWrapper.Add(pizzaWrapper);
            }

            var orderedPizzas = pizzasWrapper
                .OrderByDescending(pw => pw.OrderCount)
                .Take(20)
                .ToList();

            return orderedPizzas;
        }

        static void Print(List<PizzaWrapper> pizzasWrapper)
        {
            foreach(var pizzaWrapper in pizzasWrapper)
            {
                Console.WriteLine($"{pizzaWrapper.OrderCount}: {string.Join(", ", pizzaWrapper.Pizza.Toppings)}.");
            }
        }
    }
}
