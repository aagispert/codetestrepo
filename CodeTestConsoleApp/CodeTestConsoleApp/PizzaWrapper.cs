﻿namespace CodeTestConsoleApp
{
    public class PizzaWrapper
    {
        public Pizza Pizza { get; set; }
        public int OrderCount { get; set; }
    }
}
